<?php

/**
 * Fnutten BestCPs Plugin for XASECO
 * Version 0.69
 * Inspired by plugin.bestcps.php from galaad, but this one
 * is more efficient and has some other small improvements.
 */


Aseco::registerEvent('onSync', 'bestcps_init');
Aseco::registerEvent('onNewChallenge', 'bestcps_newChallenge');
Aseco::registerEvent("onCheckpoint", "bestcps_checkpoint");
Aseco::registerEvent('onPlayerConnect', 'bestcps_playerConnect');
Aseco::registerEvent('onPlayerDisconnect', 'bestcps_playerDisconnect');
Aseco::registerEvent('onEndRace1', 'bestcps_endRace');
Aseco::registerEvent('onPlayerManialinkPageAnswer', 'bestcps_click');

Aseco::addChatCommand('bestcps', 'Toggle show/hide bestcps');

class BestCps
{

    private $settings;
    private $checkpoints;
    private $hiddenFor;

    public function __construct($settings)
    {
        $this->settings = array();
        $this->settings['position'] = $settings['POSITION'][0]['X'][0] . " " . $settings['POSITION'][0]['Y'][0];
        $this->settings['number'] = $settings['NUMBER'][0];
        $this->settings['newline'] = $settings['NEWLINE'][0];
        $this->settings['style'] = $settings['STYLE'][0];
        $this->settings['toggle_button'] = array();
        $this->settings['toggle_button']['text'] = $settings['TOGGLE_BUTTON'][0]['TEXT'][0];
        $this->settings['toggle_button']['position'] = sprintf(
            "%f %f",
            $settings['TOGGLE_BUTTON'][0]['POSITION'][0]['X'][0],
            $settings['TOGGLE_BUTTON'][0]['POSITION'][0]['Y'][0]
        );
        $this->hiddenFor = array();
        $this->hiddenFor['live'] = array();
        $this->hiddenFor['cache'] = array();
        $this->checkpoints = array();
    }

    public function clearCps($aseco)
    {
        $this->checkpoints = array();
        $this->toggleButton($aseco);
        $this->showCheckpoints($aseco);
    }

    private function formatTime($cpnum)
    {
        $time = $this->checkpoints[$cpnum]['Time'];
        $min = (int)($time / 60000);
        $time %= 60000;
        $sec = (int)($time / 1000);
        $time %= 1000;
        $hun = (int)($time / 10);
        return sprintf('$z%02d. %d:%02d.%02d', $cpnum + 1, $min, $sec, $hun);
    }

    private function getCpXml($cpnum)
    {
        $line = (int)($cpnum / $this->settings['newline']);
        $number = $cpnum % $this->settings['newline'];
        $str = $this->formatTime($cpnum);
        $posx = $number * 11.5;
        $posy = -($line * 2.25);
        $style = 'Bgs1InRace';
        $substyle = 'NavButton';
        $nickname = handleSpecialChars($this->checkpoints[$cpnum]['Nickname']);
        if ($this->settings['style'] == 1) {
            $style = 'Icons128x32_1';
            $substyle = 'UrlBg';
        }

        $xml = sprintf(
            '<quad  posn="%f %f" sizen="11.5 2.2" halign="center" valign="center" style="%s" substyle="%s" />',
            $posx,
            $posy,
            $style,
            $substyle
        );
        $posx -= 5.25;
        $xml .= sprintf(
            '<label posn="%f %f" sizen="5.5 2" halign="left" valign="center" text="%s" />',
            $posx,
            $posy,
            $str
        );
        $posx += 4.9;
        $xml .= sprintf(
            '<label posn="%f %f" sizen="5.5 2" halign="left" valign="center" text="%s" />',
            $posx,
            $posy,
            $nickname
        );
        return $xml;
    }

    public function onCheckpoint($aseco, $param)
    {
        $maxCheckpoints = $this->settings['number'];
        $time = $param[2];
        $cp = $param[4];

        if ((!isset($this->checkpoints[$cp]) || $time < $this->checkpoints[$cp]['Time'])
        && ($cp < ($aseco->server->challenge->nbchecks - 1) && $cp < $maxCheckpoints)) {
            $nickname = $aseco->server->players->player_list[$param[1]]->nickname;
            $this->checkpoints[$cp] = array('Time' => $time, 'Nickname' => $nickname);
            $this->showCheckpoints($aseco);
        }
    }

    public function playerConnect($aseco, $player)
    {
        /**
         * on Xaseco startup hiddenFor is empty and there are no CP times
         * ToggleButton will be sent from the onNewChallenge event
         */
        if ($aseco->startup_phase) {
            return;
        }

        if (isset($this->hiddenFor['cache'][$player->login])) {
            $this->hiddenFor['live'][$player->login] = true;
            unset($this->hiddenFor['cache'][$player->login]);
        } else {
            $this->showCheckpoints($aseco, $player->login);
        }

        $this->toggleButton($aseco, $player->login);
    }

    public function playerDisconnect($player)
    {
        if (isset($this->hiddenFor['live'][$player->login])) {
            $this->hiddenFor['cache'][$player->login] = true;
            unset($this->hiddenFor['live'][$player->login]);
        }
    }

    // shows the Checkpoints to the players, also hides the checkpoints at the end of the track
    private function showCheckpoints($aseco, $login = null)
    {
        $posFrame = $this->settings['position'];
        $header = '<?xml version="1.0" encoding="UTF-8"?><manialink id="123123456">';
        $footer = '</manialink>';

        if ($login && isset($this->hiddenFor['live'][$login])) {
            $aseco->client->query("SendDisplayManialinkPageToLogin", $login, $header . $footer, 0, false);
            return;
        }

        $xml = '';
        if (!empty($this->checkpoints)) {
            $xml .= '<frame posn="' . $posFrame . '">';
            $xml .= '<format textsize="0.5"/>';
            foreach ($this->checkpoints as $num => $cp) {
                $xml .= $this->getCpXml($num);
            }
            $xml .= '</frame>';
        }

        if ($login) {
            $aseco->client->query("SendDisplayManialinkPageToLogin", $login, $header . $xml . $footer, 0, false);
        } elseif (empty($this->hiddenFor['live'])) {
            $aseco->client->query("SendDisplayManialinkPage", $header . $xml . $footer, 0, false);
        } else {
            foreach ($aseco->server->players->player_list as $player) {
                if (!in_array($player->login, array_keys($this->hiddenFor['live']))) {
                    $aseco->client->query(
                        "SendDisplayManialinkPageToLogin",
                        $player->login,
                        $header . $xml . $footer,
                        0,
                        false
                    );
                }
            }
        }
    }

    // Shows or hides the "Toggle BestCPs" Button, either to one player or all
    public function toggleButton($aseco, $login = null)
    {
        $hide = ($aseco->server->gamestate == Server::SCORE);
        $xml = '<?xml version="1.0" encoding="UTF-8"?><manialink id="123123457">';
        if (!$hide) {
            $xml .= '<frame posn="' . $this->settings['toggle_button']['position'] . '"><format textsize="0.5"/>';
            $text = $this->settings['toggle_button']['text'];
            $xml .= sprintf(
                '<label posn="0 0" sizen="%f 2" halign="left" valign="center" text="%s" action="9000" />',
                round(strlen($text) * 0.4, 2),
                $text
            );
            $xml .= '</frame>';
        }
        $xml .= '</manialink>';
    
        if ($login) {
            $aseco->client->addCall("SendDisplayManialinkPageToLogin", array($login, $xml, 0, false));
        } else {
            $aseco->client->addCall("SendDisplayManialinkPage", array($xml, 0, false));
        }
    }

    public function toggleCps($aseco, $login)
    {
        if (isset($this->hiddenFor['live'][$login])) {
            unset($this->hiddenFor['live'][$login]);
            $aseco->client->query('ChatSendServerMessageToLogin', 'Bestcps widget enabled', $login);
        } else {
            $this->hiddenFor['live'][$login] = true;
            $aseco->client->query('ChatSendServerMessageToLogin', 'Bestcps widget disabled', $login);
        }
        $this->showCheckpoints($aseco, $login);
    }

}

function bestcps_init($aseco)
{
    global $bestcps;
    $aseco->console('Loading KO settings [bestcps.xml]');
    if (!$settings = $aseco->xml_parser->parseXml('bestcps.xml')) {
        trigger_error('Could not read/parse BestCPs config file.', E_USER_ERROR);
    }
    $bestcps = new BestCps($settings['SETTINGS']);
}

function bestcps_newChallenge($aseco, $challenge)
{
    global $bestcps;
    $bestcps->toggleButton($aseco);
}

function bestcps_checkpoint($aseco, $param)
{
    global $bestcps;
    $bestcps->onCheckpoint($aseco, $param);
}

function bestcps_playerConnect($aseco, $player)
{
    global $bestcps;
    $bestcps->playerConnect($aseco, $player);
}

function bestcps_playerDisconnect($aseco, $login)
{
    global $bestcps;
    $bestcps->playerDisconnect($login);
}

function bestcps_endRace($aseco, $challenge)
{
    global $bestcps;
    $bestcps->clearCps($aseco);
}

function bestcps_click($aseco, $answer)
{
    if ($answer[2] === 9000) {
        global $bestcps;
        $login = $answer[1];

        // log clicked command
        $aseco->console('player {1} clicked command "/toggle_click "', $login);
        $bestcps->toggleCps($aseco, $login);
    }
}

function chat_bestcps($aseco, $command)
{
    global $bestcps;
    $bestcps->toggleCps($aseco, $command['author']->login);
}
